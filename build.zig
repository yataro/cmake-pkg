const std = @import("std");

// TODO: retireve version from `build.zig.zon`
// must match the version from mentioned file for now
const version_string = "0.0.12";

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "cmake-pkg",
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
    });

    const cmake_pkg_options = b.addOptions();
    cmake_pkg_options.addOption([]const u8, "version_string", version_string);
    cmake_pkg_options.addOption(
        std.SemanticVersion,
        "version",
        try std.SemanticVersion.parse(version_string),
    );
    exe.root_module.addOptions("cmake_pkg_options", cmake_pkg_options);

    const clap = b.dependency("clap", .{
        .target = target,
        .optimize = optimize,
    });
    const temp = b.dependency("temp", .{
        .target = target,
        .optimize = optimize,
    });
    exe.root_module.addImport("clap", clap.module("clap"));
    exe.root_module.addImport("temp", temp.module("temp"));

    b.installArtifact(exe);

    const tests = b.addTest(.{
        .root_source_file = b.path("src/tests.zig"),
        .target = target,
        .optimize = optimize,
    });
    const tests_options = b.addOptions();
    tests_options.addOptionArtifact("cmake_pkg_path", exe);
    tests.root_module.addOptions("tests_options", tests_options);

    const test_step = b.step("test", "Run tests");
    test_step.dependOn(&b.addRunArtifact(tests).step);
}
