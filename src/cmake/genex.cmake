# Copyright (c) 2024 Aikawa Yataro
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.



# cursed generator expressions parser and executor
# expects only valid generator expressions and doesn't support lazy evaluation

# genex function table
#
# +-------+----------------+----------+----------+
# | genex | genex function | min args | max args |
# +-------+----------------+----------+----------+

set(cmake_pkg_genex_table)

####################################################################################################
# parsing                                                                                          #
####################################################################################################

# token "text"          text data
# token "genex_start"   beginning of genex
# token "genex_sep"     genex argument separator
# token "genex_end"     end of genex

macro(cmake_pkg_genex_parse_add_text_token)
    if (NOT "${token_data}" STREQUAL "")
        list(APPEND tokens "text=${token_data}")
    endif()

    set(token_data)
endmacro()

macro(cmake_pkg_genex_parse_add_token token_type)
    list(APPEND tokens ${token_type})
    set(token_data)
endmacro()

function(cmake_pkg_genex_parse out_variable err_out_variable str)
    set(${err_out_variable} PARENT_SCOPE)

    # state "text"          initial state, waiting for $
    # state "maybe_genex"   waiting for < or return back to text
    # state "genex"         processing genex wait for : or >
    # state "genex_args"    processing genex arguments wait for , or >

    set(tokens)

    string(LENGTH "${str}" str_len)
    if (str_len EQUAL 0)
        return()
    endif()
    math(EXPR max_pos "${str_len} - 1")

    set(token_data)

    set(stack)
    list(APPEND stack "text")

    foreach(pos RANGE ${max_pos})
        string(SUBSTRING "${str}" ${pos} 1 ch)

        if (ch STREQUAL "$")
            list(APPEND stack "maybe_genex")

            continue()
        endif()

        list(GET stack -1 state)
        if (state STREQUAL "text")
            string(APPEND token_data "${ch}")
        elseif (state STREQUAL "maybe_genex")
            list(POP_BACK stack)

            if (ch STREQUAL "<")
                list(APPEND stack "genex")

                cmake_pkg_genex_parse_add_text_token()
                cmake_pkg_genex_parse_add_token("genex_start")
            else()
                string(APPEND token_data "$${ch}") # restore $ character
            endif()
        elseif (state STREQUAL "genex")
            if (ch STREQUAL ":")
                list(POP_BACK stack)
                list(APPEND stack "genex_args")

                cmake_pkg_genex_parse_add_text_token()
                cmake_pkg_genex_parse_add_token("genex_sep")
            elseif (ch STREQUAL ">")
                list(POP_BACK stack)

                cmake_pkg_genex_parse_add_text_token()
                cmake_pkg_genex_parse_add_token("genex_end")
            else()
                string(APPEND token_data "${ch}")
            endif()
        elseif (state STREQUAL "genex_args")
            if (ch STREQUAL ",")
                cmake_pkg_genex_parse_add_text_token()
                cmake_pkg_genex_parse_add_token("genex_sep")
            elseif (ch STREQUAL ">")
                list(POP_BACK stack)

                cmake_pkg_genex_parse_add_text_token()
                cmake_pkg_genex_parse_add_token("genex_end")
            else()
                string(APPEND token_data "${ch}")
            endif()
        endif()
    endforeach()

    # it's possible to fallback to text tokens but I believe we don't need this
    if (NOT stack STREQUAL "text")
        set(${err_out_variable} "Unexpected end" PARENT_SCOPE)

        return()
    endif()

    cmake_pkg_genex_parse_add_text_token() # flush remaining text

    set(${out_variable} "${tokens}" PARENT_SCOPE)
endfunction()

####################################################################################################
# execution                                                                                        #
####################################################################################################

macro(cmake_pkg_genex_pop_token)
    list(POP_FRONT tokens token)

    set(${out_variable}_data)
    string(FIND "${token}" "=" token_sep_i)
    if (NOT token_sep_i EQUAL -1)
        math(EXPR token_data_o "${token_sep_i} + 1")

        string(SUBSTRING "${token}" ${token_data_o} -1 token_data)
        string(SUBSTRING "${token}" 0 ${token_sep_i} token)

        unset(token_sep_i)
    endif()
    unset(token_sep_i)
endmacro()

function(cmake_pkg_genex_exec out_variable err_out_variable args)
    list(LENGTH args args_len)
    if (args_len EQUAL 0)
        set(${err_out_variable} "Invalid empty expression" PARENT_SCOPE)
    endif()

    list(GET args 0 genex)
    list(LENGTH cmake_pkg_genex_table table_len)

    set(i 0)
    while(i LESS table_len)
        list(SUBLIST cmake_pkg_genex_table ${i} 4 entry)

        list(GET entry 0 entry_genex)
        if (genex STREQUAL entry_genex)
            list(GET entry 1 genex_function)
            list(GET entry 2 min_args)
            list(GET entry 3 max_args)

            math(EXPR genex_args_len "${args_len} - 1")

            if (genex_args_len LESS min_args)
                set(${err_out_variable}
                    "Expected at least ${min_args} arguments for \"${genex}\"" PARENT_SCOPE)
                return()
            endif()
            if ((NOT max_args EQUAL -1) AND (genex_args_len GREATER max_args))
                set(${err_out_variable}
                    "Expected at maximum ${max_args} arguments for \"${genex}\"" PARENT_SCOPE)
                return()
            endif()

            # a little trick to handle empty string arguments
            set(args_string)
            math(EXPR max_arg_offset "${args_len} - 1")
            if (NOT max_arg_offset EQUAL 0)
                foreach(arg_offset RANGE 1 ${max_arg_offset})
                    list(GET args ${arg_offset} arg)
                    string(REGEX REPLACE "([\"$])" "\\1" arg_encoded "${arg}")
                    string(APPEND args_string " \"${arg_encoded}\"")
                endforeach()
            endif()

            cmake_language(EVAL CODE "${genex_function}(result error ${args_string})")
            if (error)
                set(${err_out_variable} "${error}" PARENT_SCOPE)
            else()
                set(${out_variable} "${result}" PARENT_SCOPE)
            endif()

            return()
        endif()

        math(EXPR i "${i} + 4")
    endwhile()

    set(${err_out_variable} "Unknown expression \"${genex}\"" PARENT_SCOPE)
endfunction()

function(cmake_pkg_genex_enter out_variable err_out_variable)
    set(arg)
    set(args)

    set(prev_token)
    while(tokens)
        cmake_pkg_genex_pop_token()
        set(tokens "${tokens}" PARENT_SCOPE)

        if (token STREQUAL "text")
            string(APPEND arg "${token_data}")
        elseif (token STREQUAL "genex_start")
            cmake_pkg_genex_enter(result error)
            if (error)
                set(${err_out_variable} "${error}" PARENT_SCOPE)
                return()
            endif()

            string(APPEND arg "${result}")
        elseif (token STREQUAL "genex_sep")
            list(APPEND args "${arg}")
            set(arg)
        elseif (token STREQUAL "genex_end")
            if (NOT "${arg}" STREQUAL "" OR prev_token STREQUAL "genex_sep")
                list(APPEND args "${arg}")
            endif()

            cmake_pkg_genex_exec(result error "${args}")
            if (error)
                set(${err_out_variable} "${error}" PARENT_SCOPE)
                return()
            endif()

            set(${out_variable} "${result}" PARENT_SCOPE)

            return()
        endif()

        set(prev_token "${token}")
    endwhile()

    set(${err_out_variable} "Unmatched expression" PARENT_SCOPE)
endfunction()



function(cmake_pkg_genex_eval out_variable str)
    cmake_pkg_genex_parse(tokens error "${str}")
    if (error)
        message(FATAL_ERROR "${error} in expression \"${str}\"")
    endif()

    set(result)
    while(tokens)
        cmake_pkg_genex_pop_token()

        if (token STREQUAL "text")
            string(APPEND result "${token_data}")
        elseif (token STREQUAL "genex_start")
            cmake_pkg_genex_enter(genex_result error)
            if (error)
                message(FATAL_ERROR "${error}: \"${str}\"")
            endif()

            string(APPEND result "${genex_result}")
        else()
            message(FATAL_ERROR "Unexpected token \"${token}\" in expression \"${str}\"")
        endif()
    endwhile()

    set(${out_variable} "${result}" PARENT_SCOPE)
endfunction()

####################################################################################################
# genex implementation                                                                             #
####################################################################################################

macro(cmake_pkg_genex_check_condition condition err_out_variable)
    if (NOT (condition EQUAL 1 OR condition EQUAL 0))
        set(${err_out_variable} "Invalid condition \"${condition}\"" PARENT_SCOPE)
        return()
    endif()
endmacro()

function(cmake_pkg_genex_1 out_variable err_out_variable str)
    set(${out_variable} "${str}" PARENT_SCOPE)
endfunction()

function(cmake_pkg_genex_0 out_variable err_out_variable ignored)
    set(${out_variable} "" PARENT_SCOPE)
endfunction()

function(cmake_pkg_genex_if out_variable err_out_variable condition true_str false_str)
    cmake_pkg_genex_check_condition("${condition}" ${err_out_variable})
    if (condition EQUAL 1)
        set(${out_variable} "${true_str}" PARENT_SCOPE)
    else()
        set(${out_variable} "${false_str}" PARENT_SCOPE)
    endif()
endfunction()

function(cmake_pkg_genex_bool out_variable err_out_variable value)
    if (value)
        set(${out_variable} "1" PARENT_SCOPE)
    else()
        set(${out_variable} "0" PARENT_SCOPE)
    endif()
endfunction()

function(cmake_pkg_genex_and out_variable err_out_variable)
    if (ARGC LESS 3)
        set(${err_out_variable} "Invalid AND" PARENT_SCOPE)
        return()
    endif()

    list(SUBLIST ARGV 2 -1 conditions)
    foreach(condition IN LISTS conditions)
        cmake_pkg_genex_check_condition("${condition}" ${err_out_variable})

        if (condition EQUAL 0)
            set(${out_variable} "0" PARENT_SCOPE)
            return()
        endif()
    endforeach()

    set(${out_variable} "1" PARENT_SCOPE)
endfunction()

function(cmake_pkg_genex_or out_variable err_out_variable)
    if (ARGC LESS 3)
        set(${err_out_variable} "Invalid OR" PARENT_SCOPE)
        return()
    endif()

    list(SUBLIST ARGV 2 -1 conditions)
    foreach(condition IN LISTS conditions)
        cmake_pkg_genex_check_condition("${condition}" ${err_out_variable})

        if (condition EQUAL 1)
            set(${out_variable} "1" PARENT_SCOPE)
            return()
        endif()
    endforeach()

    set(${out_variable} "0" PARENT_SCOPE)
endfunction()

function(cmake_pkg_genex_not out_variable err_out_variable condition)
    cmake_pkg_genex_check_condition("${condition}" ${err_out_variable})
    if (condition EQUAL 1)
        set(${out_variable} "0" PARENT_SCOPE)
    else()
        set(${out_variable} "1" PARENT_SCOPE)
    endif()
endfunction()

function(cmake_pkg_genex_strequal out_variable err_out_variable str1 str2)
    if (str1 STREQUAL str2)
        set(${out_variable} "1" PARENT_SCOPE)
    else()
        set(${out_variable} "0" PARENT_SCOPE)
    endif()
endfunction()

function(cmake_pkg_genex_equal out_variable err_out_variable value1 value2)
    if (value1 EQUAL value2)
        set(${out_variable} "1" PARENT_SCOPE)
    else()
        set(${out_variable} "0" PARENT_SCOPE)
    endif()
endfunction()

function(cmake_pkg_genex_version_less out_variable err_out_variable v1 v2)
    if (v1 VERSION_LESS v2)
        set(${out_variable} "1" PARENT_SCOPE)
    else()
        set(${out_variable} "0" PARENT_SCOPE)
    endif()
endfunction()

function(cmake_pkg_genex_version_greater out_variable err_out_variable v1 v2)
    if (v1 VERSION_GREATER v2)
        set(${out_variable} "1" PARENT_SCOPE)
    else()
        set(${out_variable} "0" PARENT_SCOPE)
    endif()
endfunction()

function(cmake_pkg_genex_version_equal out_variable err_out_variable v1 v2)
    if (v1 VERSION_EQUAL v2)
        set(${out_variable} "1" PARENT_SCOPE)
    else()
        set(${out_variable} "0" PARENT_SCOPE)
    endif()
endfunction()

function(cmake_pkg_genex_version_less_equal out_variable err_out_variable v1 v2)
    if (v1 VERSION_LESS_EQUAL v2)
        set(${out_variable} "1" PARENT_SCOPE)
    else()
        set(${out_variable} "0" PARENT_SCOPE)
    endif()
endfunction()

function(cmake_pkg_genex_version_greater_equal out_variable err_out_variable v1 v2)
    if (v1 VERSION_GREATER_EQUAL v2)
        set(${out_variable} "1" PARENT_SCOPE)
    else()
        set(${out_variable} "0" PARENT_SCOPE)
    endif()
endfunction()

function(cmake_pkg_genex_lower_case out_variable err_out_variable str)
    string(TOLOWER "${str}" str)
    set(${out_variable} "${str}" PARENT_SCOPE)
endfunction()

function(cmake_pkg_genex_upper_case out_variable err_out_variable str)
    string(TOUPPER "${str}" str)
    set(${out_variable} "${str}" PARENT_SCOPE)
endfunction()

function(cmake_pkg_genex_make_c_identifier out_variable err_out_variable str)
    string(MAKE_C_IDENTIFIER "${str}" str)
    set(${out_variable} "${str}" PARENT_SCOPE)
endfunction()

function(cmake_pkg_genex_in_list out_variable err_out_variable str str_list)
    list(FIND str_list "${str}" str_i)
    if (str_i EQUAL -1)
        set(${out_variable} "0" PARENT_SCOPE)
    else()
        set(${out_variable} "1" PARENT_SCOPE)
    endif()
endfunction()

function(cmake_pkg_genex_config out_variable err_out_variable)
    if (ARGC EQUAL 2)
        set(${out_variable} "${CMAKE_BUILD_TYPE}" PARENT_SCOPE)
        return()
    endif()

    list(SUBLIST ARGV 2 -1 configs)
    list(FIND configs "${CMAKE_BUILD_TYPE}" config_i)
    if (config_i EQUAL -1)
        set(${out_variable} "0" PARENT_SCOPE)
        return()
    endif()

    set(${out_variable} "1" PARENT_SCOPE)
endfunction()

function(cmake_pkg_genex_link_only out_variable err_out_variable str)
    if (cmake_pkg_LINK_ONLY)
        set(${out_variable} "${str}" PARENT_SCOPE)
    else()
        set(${out_variable} "" PARENT_SCOPE)
    endif()
endfunction()

function(cmake_pkg_genex_target_exists out_variable err_out_variable tgt)
    if (TARGET "${tgt}")
        set(${out_variable} "1" PARENT_SCOPE)
    else()
        set(${out_variable} "0" PARENT_SCOPE)
    endif()
endfunction()

function(cmake_pkg_genex_target_name_if_exists out_variable err_out_variable tgt)
    if (TARGET "${tgt}")
        set(${out_variable} "${tgt}" PARENT_SCOPE)
    else()
        set(${out_variable} "" PARENT_SCOPE)
    endif()
endfunction()

function(cmake_pkg_genex_target_policy out_variable err_out_variable unused)
    set(${out_variable} "" PARENT_SCOPE)
endfunction()

function(cmake_pkg_genex_target_property out_variable err_out_variable)
    set(${out_variable} "" PARENT_SCOPE)

    if (ARGC EQUAL 4)
        get_target_property(prop "${ARGV2}" "${ARGV3}")
        if (prop)
            set(${out_variable} "${prop}" PARENT_SCOPE)
        endif()
    endif()
endfunction()

function(cmake_pkg_genex_angle_r out_variable err_out_variable)
    set(${out_variable} ">" PARENT_SCOPE)
endfunction()

function(cmake_pkg_genex_comma out_variable err_out_variable)
    set(${out_variable} "," PARENT_SCOPE)
endfunction()

function(cmake_pkg_genex_semicolon out_variable err_out_variable)
    set(${out_variable} ";" PARENT_SCOPE)
endfunction()

list(APPEND cmake_pkg_genex_table
    "1"                         cmake_pkg_genex_1                       1   1
    "0"                         cmake_pkg_genex_0                       1   1
    "IF"                        cmake_pkg_genex_if                      3   3
    "BOOL"                      cmake_pkg_genex_bool                    1   1
    "AND"                       cmake_pkg_genex_and                     1   -1
    "OR"                        cmake_pkg_genex_or                      1   -1
    "NOT"                       cmake_pkg_genex_not                     1   1
    "STREQUAL"                  cmake_pkg_genex_strequal                2   2
    "EQUAL"                     cmake_pkg_genex_equal                   2   2
    "VERSION_LESS"              cmake_pkg_genex_version_less            2   2
    "VERSION_GREATER"           cmake_pkg_genex_version_greater         2   2
    "VERSION_EQUAL"             cmake_pkg_genex_version_equal           2   2
    "VERSION_LESS_EQUAL"        cmake_pkg_genex_version_less_equal      2   2
    "VERSION_GREATER_EQUAL"     cmake_pkg_genex_version_greater_equal   2   2
    "LOWER_CASE"                cmake_pkg_genex_lower_case              1   1
    "UPPER_CASE"                cmake_pkg_genex_upper_case              1   1
    "MAKE_C_IDENTIFIER"         cmake_pkg_genex_make_c_identifier       1   1
    "IN_LIST"                   cmake_pkg_genex_in_list                 2   2
    "CONFIG"                    cmake_pkg_genex_config                  0   -1
    "LINK_ONLY"                 cmake_pkg_genex_link_only               1   1
    "TARGET_EXISTS"             cmake_pkg_genex_target_exists           1   1
    "TARGET_NAME_IF_EXISTS"     cmake_pkg_genex_target_name_if_exists   1   1
    "TARGET_POLICY"             cmake_pkg_genex_target_policy           1   1
    "TARGET_PROPERTY"           cmake_pkg_genex_target_property         1   2
    "ANGLE-R"                   cmake_pkg_genex_angle_r                 0   0
    "COMMA"                     cmake_pkg_genex_comma                   0   0
    "SEMICOLON"                 cmake_pkg_genex_semicolon               0   0
)
