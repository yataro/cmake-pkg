const std = @import("std");

pub fn encode_arg(allocator: std.mem.Allocator, arg: []const u8) !?[]u8 {
    var encode = false;

    for (arg) |c| {
        switch (c) {
            // double \ in case of windows is fine
            '\\', ' ', '"', '\t', '\n' => {
                encode = true;
                break;
            },
            else => {},
        }
    }

    if (!encode) {
        return null;
    }

    var encoded = std.ArrayList(u8).init(allocator);
    errdefer encoded.deinit();
    try encoded.ensureTotalCapacity(arg.len);

    try encoded.append('"');
    for (arg) |c| {
        switch (c) {
            '\\', '"' => {
                try encoded.appendSlice(&.{ '\\', c });
            },
            else => {
                try encoded.append(c);
            },
        }
    }
    try encoded.append('"');
    encoded.shrinkAndFree(encoded.items.len);

    return encoded.items;
}

pub fn sync_stdio_pipe(
    allocator: std.mem.Allocator,
    stdout: std.fs.File,
    stderr: std.fs.File,
    out: std.fs.File.Writer,
) !void {
    const StreamEnum = enum {
        stdout,
        stderr,
    };

    var poller = std.io.poll(allocator, StreamEnum, .{
        .stdout = stdout,
        .stderr = stderr,
    });
    defer poller.deinit();

    while (try poller.poll()) {
        for (&poller.fifos) |*fifo| {
            while (fifo.readableLength() != 0) {
                const readable = fifo.readableSlice(0);
                _ = try out.write(readable);
                fifo.discard(readable.len);
            }
        }
    }
}

fn test_argument_encoding(arg: []const u8, expected: []const u8) !void {
    const encoded = try encode_arg(std.testing.allocator, arg);
    if (encoded) |value| {
        defer std.testing.allocator.free(value);

        try std.testing.expectEqualStrings(expected, value);
    } else {
        try std.testing.expectEqualStrings(arg, expected);
    }
}

test "argument encoding" {
    try test_argument_encoding("simple", "simple");
    try test_argument_encoding("with\"quote", "\"with\\\"quote\"");
    try test_argument_encoding("with\n spaces\t", "\"with\n spaces\t\"");
    try test_argument_encoding("with\\backslash", "\"with\\\\backslash\"");

    _ = try encode_arg(std.testing.failing_allocator, "simple");
    try std.testing.expectError(
        error.OutOfMemory,
        encode_arg(std.testing.failing_allocator, "with space"),
    );
}
