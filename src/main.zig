const clap = @import("clap");
const std = @import("std");
const builtin = @import("builtin");
const out = @import("out.zig");
const cmake = @import("cmake.zig");
const utils = @import("utils.zig");
const cmake_pkg_options = @import("cmake_pkg_options");

const EXIT_FAILURE = 1;
const EXIT_SUCCESS = 0;

const usage_str =
    \\Usage:
    \\    cmake-pkg [OPTIONS] <TARGET>...
    \\Options:
;

const params_str =
    \\    -h, --help              print this message and exit
    \\    -V, --version           print cmake-pkg version and exit
    \\    --cflags                print compiler flags
    \\    --cflags-only-I         print include-dir flags
    \\    --cflags-only-D         print define flags
    \\    --libs                  print linker flags
    \\    --libs-only-L           print LDPATH linker flags
    \\    --libs-only-l           print LIBNAME linker flags
    \\    --convert-libs          convert libraries from absolute paths to -L -l form
    \\    --with-path <PATH>...   add a directory to the CMAKE_PREFIX_PATH
    \\    --config <CONFIG>...    specify configuation (defaults to release or any other available)
    \\    -D <VARIABLE>...        define variable entry
    \\    -s, --silent            silent mode
    \\    --msvc                  print flags in MSVC syntax
    \\    <TARGET>...             target(s) in format Package::target
;

const configs = std.StaticStringMap(cmake.GetFlagsOptions.Config).initComptime(.{
    .{ "release", cmake.GetFlagsOptions.Config.Release },
    .{ "debug", cmake.GetFlagsOptions.Config.Debug },
    .{ "none", cmake.GetFlagsOptions.Config.None },
    .{ "relwithdebinfo", cmake.GetFlagsOptions.Config.RelWithDebInfo },
    .{ "minsizerel", cmake.GetFlagsOptions.Config.MinSizeRel },
});

const allowed_configs_str = blk: {
    var str: []const u8 = "";
    const sep: []const u8 = ", ";

    for (configs.keys()) |key| {
        str = str ++ sep ++ key;
    }

    break :blk str[sep.len..];
};

fn print_help() !void {
    try out.e_print("{s}\n{s}\n", .{ usage_str, params_str });
}

fn print_version() !void {
    try out.e_print("cmake-pkg {s} (zig {s})\n", .{
        cmake_pkg_options.version_string,
        builtin.zig_version_string,
    });
}

pub fn main() !u8 {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const gpa_allocator = gpa.allocator();

    var arena = std.heap.ArenaAllocator.init(gpa_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const params = comptime clap.parseParamsComptime(params_str);
    const parsers = comptime .{
        .TARGET = clap.parsers.string,
        .PATH = clap.parsers.string,
        .VARIABLE = clap.parsers.string,
        .CONFIG = clap.parsers.string,
    };

    var diag = clap.Diagnostic{};
    const opts = clap.parse(clap.Help, &params, parsers, .{
        .allocator = allocator,
        .diagnostic = &diag,
    }) catch |err| {
        try diag.report(out.e_writer(), err);

        return EXIT_FAILURE;
    };

    if (opts.args.help != 0) {
        try print_help();

        return EXIT_SUCCESS;
    }

    if (opts.args.version != 0) {
        try print_version();

        return EXIT_SUCCESS;
    }

    var options = cmake.GetFlagsOptions{};
    if (opts.args.cflags != 0) {
        options.cflags_include = true;
        options.cflags_define = true;
    }
    if (opts.args.@"cflags-only-I" != 0) {
        options.cflags_include = true;
    }
    if (opts.args.@"cflags-only-D" != 0) {
        options.cflags_define = true;
    }

    if (opts.args.libs != 0) {
        options.libs_ldpath = true;
        options.libs_lib = true;
    }
    if (opts.args.@"libs-only-L" != 0) {
        options.libs_ldpath = true;
    }
    if (opts.args.@"libs-only-l" != 0) {
        options.libs_lib = true;
    }
    if (opts.args.@"convert-libs" != 0) {
        options.convert_libs = true;
    }

    options.paths = opts.args.@"with-path";

    var variables = std.ArrayList(cmake.Variable).init(allocator);
    try variables.resize(opts.args.D.len);

    for (opts.args.D, 0..) |variable, i| {
        variables.items[i] = cmake.parse_variable(variable) catch {
            try out.e_print(
                \\Can't parse variable {s}
                \\Should be: VAR=value
                \\
            , .{variable});

            return EXIT_FAILURE;
        };
    }

    options.vars = variables.items;

    if (opts.args.config.len != 0) {
        const config_str = opts.args.config[opts.args.config.len - 1];
        const config = configs.get(config_str) orelse {
            try out.e_print(
                \\Invalid config "{s}"
                \\Allowed values: {s}
                \\
            , .{ config_str, allowed_configs_str });

            return EXIT_FAILURE;
        };

        options.config = config;
    }

    if (opts.args.silent != 0) {
        options.silent = true;
    }

    if (opts.args.msvc != 0) {
        options.msvc = true;
    }

    if (opts.positionals.len == 0) {
        return EXIT_SUCCESS;
    }

    var targets = std.ArrayList(cmake.Target).init(allocator);
    try targets.resize(opts.positionals.len);

    for (opts.positionals, 0..) |target, i| {
        targets.items[i] = cmake.parse_target(target) catch {
            try out.e_print(
                \\Invalid target {s}
                \\Should be: Package::Target
                \\
            , .{target});

            return EXIT_FAILURE;
        };
    }

    const flags = cmake.get_flags(
        allocator,
        targets.items,
        options,
    ) catch |err| {
        switch (err) {
            cmake.Error.CMakeError => {
                try out.e_print(
                    \\CMake error.
                    \\Check the CMake output for more details.
                    \\
                , .{});
            },
            cmake.Error.InvalidDependenciesJSON => {
                try out.e_print(
                    \\Invalid dependencies generated.
                    \\Please report this with a minimal reproducible example.
                    \\
                , .{});
            },
            cmake.Error.InvalidLibrary => {
                try out.e_print(
                    \\Encountered invalid library.
                    \\The cause could be a bugged config file or cmake-pkg bug.
                    \\If you believe this is a cmake-pkg bug, please report it with a minimal reproducible example.
                    \\
                , .{});
            },
            error.FileTooBig => {
                try out.e_print(
                    \\Generated file is too big.
                    \\The cause could be a very large dependency tree or cmake-pkg bug.
                    \\If you believe this is a cmake-pkg bug, please report it with a minimal reproducible example.
                    \\
                , .{});
            },
            error.OutOfMemory => {
                try out.e_print("Out of memory.\n", .{});
            },
            else => {
                return err;
            },
        }
        return EXIT_FAILURE;
    };

    if (flags.flags.len != 0) {
        const arg_array = try allocator.alloc([]const u8, flags.flags.len);

        for (flags.flags, 0..) |flag, i| {
            const encoded = try utils.encode_arg(allocator, flag);
            if (encoded) |value| {
                arg_array[i] = value;
            } else {
                arg_array[i] = flag;
            }
        }

        const args_str = try std.mem.join(allocator, " ", arg_array);
        try out.print("{s}\n", .{args_str});
    }

    return EXIT_SUCCESS;
}
