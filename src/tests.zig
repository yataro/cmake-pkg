const std = @import("std");
const tests_options = @import("tests_options");

// reference all files with tests
comptime {
    _ = @import("./cmake.zig");
    _ = @import("./utils.zig");
}

fn write_test_file(dir: std.fs.Dir, comptime filename: []const u8) !void {
    var file = try dir.createFile(
        comptime std.fs.path.basename(filename),
        .{},
    );
    defer file.close();

    const contents = @embedFile(filename);
    try file.writeAll(contents);
}

fn run_process(argv: []const []const u8, cwd: ?std.fs.Dir, stdout: ?*[]u8) !void {
    var process = std.process.Child.init(argv, std.testing.allocator);
    process.cwd_dir = cwd;

    process.stdin_behavior = .Ignore;
    if (stdout != null) {
        process.stdout_behavior = .Pipe;
    } else {
        process.stdout_behavior = .Ignore;
    }

    try process.spawn();
    if (stdout) |stdout_ptr| {
        stdout_ptr.* = try process.stdout.?.readToEndAlloc(
            std.testing.allocator,
            std.math.maxInt(usize),
        );
    }

    const result = try process.wait();
    switch (result) {
        .Exited => |code| {
            if (code == 0) {
                return;
            }
        },
        else => {},
    }

    return error.TestChildProcessFailed;
}

fn test_cmake_pkg_cli(expected: []const u8, comptime args: []const []const u8) !void {
    var tmpdir = std.testing.tmpDir(.{});
    defer tmpdir.cleanup();

    const tmpdir_handle = tmpdir.dir;
    try write_test_file(tmpdir_handle, "test/CMakePKGTestConfig.cmake");

    const test_dir_path = try tmpdir_handle.realpathAlloc(std.testing.allocator, ".");
    defer std.testing.allocator.free(test_dir_path);

    var stdout: []u8 = undefined;
    try run_process(
        &[_][]const u8{
            tests_options.cmake_pkg_path,
        } ++ args ++ &[_][]const u8{
            "--silent",
            "--with-path",
            test_dir_path,
            "CMakePKGTest::CMakePKGTest",
        },
        tmpdir_handle,
        &stdout,
    );
    defer std.testing.allocator.free(stdout);

    try std.testing.expectEqualStrings(expected, stdout);
}

test "CMake generator expressions" {
    var tmpdir = std.testing.tmpDir(.{});
    defer tmpdir.cleanup();

    const tmpdir_handle = tmpdir.dir;
    try write_test_file(tmpdir_handle, "test/CMakeLists.txt");
    try write_test_file(tmpdir_handle, "cmake/genex.cmake");

    try run_process(&.{ "cmake", "." }, tmpdir_handle, null);
}

test "cmake-pkg cli" {
    try test_cmake_pkg_cli(
        "-I/cmakepkgtest -I/cmakepkgtestimpl\n",
        &.{"--cflags-only-I"},
    );
    try test_cmake_pkg_cli(
        "-Dcmakepkgtest -DIS_MAIN=1 -Dcmakepkgtestimpl -DIS_IMPL=1\n",
        &.{"--cflags-only-D"},
    );
    try test_cmake_pkg_cli(
        "-I/cmakepkgtest -Dcmakepkgtest -DIS_MAIN=1 -I/cmakepkgtestimpl -Dcmakepkgtestimpl -DIS_IMPL=1\n",
        &.{"--cflags"},
    );

    try test_cmake_pkg_cli(
        "/I/cmakepkgtest /I/cmakepkgtestimpl\n",
        &.{ "--msvc", "--cflags-only-I" },
    );
    try test_cmake_pkg_cli(
        "/Dcmakepkgtest /DIS_MAIN=1 /Dcmakepkgtestimpl /DIS_IMPL=1\n",
        &.{ "--msvc", "--cflags-only-D" },
    );
    try test_cmake_pkg_cli(
        "/I/cmakepkgtest /Dcmakepkgtest /DIS_MAIN=1 /I/cmakepkgtestimpl /Dcmakepkgtestimpl /DIS_IMPL=1\n",
        &.{ "--msvc", "--cflags" },
    );

    try test_cmake_pkg_cli(
        "",
        &.{"--libs-only-L"},
    );
    try test_cmake_pkg_cli(
        "/libcmakepkgtest.a /libcmakepkgtestimpl.a\n",
        &.{"--libs-only-l"},
    );
    try test_cmake_pkg_cli(
        "/libcmakepkgtest.a /libcmakepkgtestimpl.a\n",
        &.{"--libs"},
    );
    try test_cmake_pkg_cli(
        "-L/\n",
        &.{ "--convert-libs", "--libs-only-L" },
    );
    try test_cmake_pkg_cli(
        "-L/ -l:libcmakepkgtest.a -l:libcmakepkgtestimpl.a\n",
        &.{ "--convert-libs", "--libs" },
    );

    try test_cmake_pkg_cli(
        "",
        &.{ "--msvc", "--libs-only-L" },
    );
    try test_cmake_pkg_cli(
        "/libcmakepkgtest.a /libcmakepkgtestimpl.a\n",
        &.{ "--msvc", "--libs-only-l" },
    );
    try test_cmake_pkg_cli(
        "/libcmakepkgtest.a /libcmakepkgtestimpl.a\n",
        &.{ "--msvc", "--libs" },
    );
    try test_cmake_pkg_cli(
        "/LIBPATH:/\n",
        &.{ "--msvc", "--convert-libs", "--libs-only-L" },
    );
    try test_cmake_pkg_cli(
        "/LIBPATH:/ libcmakepkgtest.a libcmakepkgtestimpl.a\n",
        &.{ "--msvc", "--convert-libs", "--libs" },
    );

    try test_cmake_pkg_cli(
        "-I/cmakepkgtest -Dcmakepkgtest -DIS_MAIN=1 /libcmakepkgtest.a -I/cmakepkgtestimpl -Dcmakepkgtestimpl -DIS_IMPL=1 /libcmakepkgtestimpl.a\n",
        &.{ "--cflags", "--libs" },
    );

    try test_cmake_pkg_cli(
        "/I/cmakepkgtest /Dcmakepkgtest /DIS_MAIN=1 /libcmakepkgtest.a /I/cmakepkgtestimpl /Dcmakepkgtestimpl /DIS_IMPL=1 /libcmakepkgtestimpl.a\n",
        &.{ "--msvc", "--cflags", "--libs" },
    );
}
