const std = @import("std");

pub fn writer() std.fs.File.Writer {
    return std.io.getStdOut().writer();
}

pub fn e_writer() std.fs.File.Writer {
    return std.io.getStdErr().writer();
}

pub fn print(comptime format: []const u8, args: anytype) !void {
    try writer().print(format, args);
}

pub fn e_print(comptime format: []const u8, args: anytype) !void {
    try e_writer().print(format, args);
}
