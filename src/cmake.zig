const std = @import("std");
const builtin = @import("builtin");
const temp = @import("temp");
const out = @import("./out.zig");
const utils = @import("utils.zig");

const target_separator = "::";
const variable_separator = "=";
const valid_characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_";
const list_separator = ";";

const cmake_lists_filename = "CMakeLists.txt";
const genex_filename = "genex.cmake";
const targets_filename = "targets.json";
const cache_filename = "cache.cmake";
const dependencies_filename = "dependencies.json";

const cmake_lists_content = @embedFile("cmake/" ++ cmake_lists_filename);
const genex_content = @embedFile("cmake/" ++ genex_filename);

const I_prefix = "-I";
const I_prefix_msvc = "/I";

const D_prefix = "-D";
const D_prefix_msvc = "/D";

const L_prefix = "-L";
const L_prefix_msvc = "/LIBPATH:";

const l_prefix = "-l";
const l_prefix_full_name = "-l:";
const l_prefix_msvc = "";
const l_prefix_full_name_msvc = "";

const color_env_var = "CLICOLOR_FORCE";

const dependencies_max_bytes = 1048576; // 1MiB is enough I suppose

pub const Error = error{
    InvalidTarget,
    InvalidVariable,
    CMakeError,
    InvalidDependenciesJSON,
    InvalidLibrary,
};

pub const Target = struct {
    package: []const u8,
    name: []const u8,
};

pub const Variable = struct {
    name: []const u8,
    value: []const u8,
};

pub const GetFlagsOptions = struct {
    pub const Config = enum {
        Any,
        Release,
        Debug,
        None,
        RelWithDebInfo,
        MinSizeRel,
    };

    cflags_include: bool = false,
    cflags_define: bool = false,
    libs_ldpath: bool = false,
    libs_lib: bool = false,
    convert_libs: bool = false,
    paths: []const []const u8 = &.{},
    vars: []const Variable = &.{},
    config: Config = .Any,
    silent: bool = false,
    msvc: bool = false,
};

const DependencyLib = struct {
    lib: []const u8,
    link_only: bool,
};

const DependencyTarget = struct {
    include_dirs: [][]const u8,
    defines: [][]const u8,
    library: ?[]const u8,
    link_libraries: []DependencyLib,
    link_dirs: [][]const u8,
};

const TargetMap = std.StringHashMap(DependencyTarget);

const Dependencies = struct {
    dependencies: [][]const u8,
    targets: TargetMap,
};

const ParsedDependencies = struct {
    arena: std.heap.ArenaAllocator,
    value: Dependencies,

    pub fn deinit(self: ParsedDependencies) void {
        self.arena.deinit();
    }
};

const Library = struct {
    l: []const u8,
    L: []const u8,
};

const FlagSet = std.StringArrayHashMap(void);

const GetFlagsContext = struct {
    allocator: std.mem.Allocator,
    flags: *FlagSet,
    options: GetFlagsOptions,
};

pub const Flags = struct {
    arena: std.heap.ArenaAllocator,
    flags: [][]const u8,

    pub fn deinit(self: Flags) void {
        self.arena.deinit();
    }
};

fn validate_target_string(string: []const u8) !void {
    if (string.len == 0) {
        return Error.InvalidTarget;
    }

    for (string) |character| {
        if (std.mem.indexOfScalar(u8, valid_characters, character) == null) {
            return Error.InvalidTarget;
        }
    }
}

pub fn validate_target(target: Target) !void {
    try validate_target_string(target.package);

    const i = std.mem.indexOf(u8, target.name, target_separator);
    if (i) |value| {
        try validate_target_string(target.name[0..value]);
        try validate_target_string(target.name[value + target_separator.len ..]);
    } else {
        try validate_target_string(target.name);
    }
}

pub fn parse_target(target: []const u8) !Target {
    const i = std.mem.indexOf(u8, target, target_separator) orelse {
        return Error.InvalidTarget;
    };

    const target_offset = i + target_separator.len;

    if (i == 0 or target_offset == target.len) {
        return Error.InvalidTarget;
    }

    const package = target[0..i];
    const name = target[target_offset..];

    const parsed_target = Target{ .package = package, .name = name };

    try validate_target(parsed_target);

    return parsed_target;
}

pub fn parse_variable(variable: []const u8) !Variable {
    if (variable.len == 0) {
        return Error.InvalidVariable;
    }

    const sep_i = std.mem.indexOf(u8, variable, variable_separator) orelse {
        return Error.InvalidVariable;
    };

    if (sep_i == 0) {
        return Error.InvalidVariable;
    }

    const name = variable[0..sep_i];
    const value = variable[sep_i + 1 ..];

    return .{
        .name = name,
        .value = value,
    };
}

fn encode_cmake_string(allocator: std.mem.Allocator, str: []const u8) ![]u8 {
    var encoded = std.ArrayList(u8).init(allocator);
    errdefer encoded.deinit();
    // I believe there's no use for `initCapacity` due to `ensureTotalCapacityPrecise`
    // `ensureTotalCapacity` will work better because we might push some extra characters
    try encoded.ensureTotalCapacity(str.len + 2);

    try encoded.append('"');
    for (str) |c| {
        if (c == '"') {
            try encoded.appendSlice("\\\"");
        } else {
            try encoded.append(c);
        }
    }
    try encoded.append('"');

    return encoded.toOwnedSlice();
}

fn generate_targets_json(allocator: std.mem.Allocator, targets: []const Target, options: GetFlagsOptions) ![]u8 {
    var targets_array = std.ArrayList(Target).init(allocator);
    defer targets_array.deinit();

    target_loop: for (targets, 0..) |target, i| {
        // ignore duplicates
        for (targets, 0..) |t, j| {
            if (std.mem.eql(
                u8,
                target.package,
                t.package,
            ) and std.mem.eql(
                u8,
                target.name,
                t.name,
            )) {
                if (i == j) {
                    break;
                }

                continue :target_loop;
            }
        }

        try targets_array.append(target);
    }

    const config = switch (options.config) {
        .Any => "ANY",
        .Release => "RELEASE",
        .Debug => "DEBUG",
        .None => "NONE",
        .RelWithDebInfo => "RELWITHDEBINFO",
        .MinSizeRel => "MINSIZEREL",
    };

    const json = try std.json.stringifyAlloc(
        allocator,
        .{
            .config = config,
            .targets = targets_array.items,
        },
        .{},
    );

    return json;
}

fn generate_initial_cache(allocator: std.mem.Allocator, options: GetFlagsOptions) ![]u8 {
    var arena = std.heap.ArenaAllocator.init(allocator);
    defer arena.deinit();
    const arena_allocator = arena.allocator();

    var cache = std.ArrayList(u8).init(allocator);
    errdefer cache.deinit();

    var writer = cache.writer();

    const path = try std.mem.join(arena_allocator, list_separator, options.paths);

    if (path.len != 0) {
        const encoded_path = try encode_cmake_string(arena_allocator, path);

        try writer.print("set(CMAKE_PREFIX_PATH {s} CACHE PATH \"\")\n", .{encoded_path});
    }

    for (options.vars) |variable| {
        const encoded_name = try encode_cmake_string(arena_allocator, variable.name);
        const encoded_value = try encode_cmake_string(arena_allocator, variable.value);

        try writer.print("set({s} {s} CACHE INTERNAL \"\")\n", .{ encoded_name, encoded_value });
    }

    return cache.toOwnedSlice();
}

fn call_cmake(allocator: std.mem.Allocator, dir: std.fs.Dir, silent: bool) !void {
    var process = std.process.Child.init(
        &.{ "cmake", "-C", cache_filename, "." },
        allocator,
    );
    process.cwd_dir = dir;

    var env = try std.process.getEnvMap(allocator);
    defer env.deinit();

    process.env_map = &env;

    if (silent) {
        process.stdout_behavior = .Ignore;
        process.stderr_behavior = .Ignore;
    } else {
        // preserve color when piping to tty's stderr
        if (env.get(color_env_var) == null and std.io.getStdErr().isTty()) {
            try env.put(color_env_var, "1");
        }

        process.stdout_behavior = .Pipe;
        process.stderr_behavior = .Pipe;
    }

    try process.spawn();

    if (!silent) {
        try utils.sync_stdio_pipe(
            allocator,
            process.stdout.?,
            process.stderr.?,
            out.e_writer(),
        );
    }

    const result = try process.wait();
    switch (result) {
        .Exited => |code| {
            if (code != 0) {
                return Error.CMakeError;
            }
        },
        else => {
            return Error.CMakeError;
        },
    }
}

fn parse_dependencies_json(allocator: std.mem.Allocator, json: []const u8) !ParsedDependencies {
    var arena = std.heap.ArenaAllocator.init(allocator);
    errdefer arena.deinit();
    const arena_allocator = arena.allocator();

    var dependencies = Dependencies{
        .dependencies = undefined,
        .targets = std.StringHashMap(DependencyTarget).init(arena_allocator),
    };

    const value = try std.json.parseFromSliceLeaky(
        std.json.Value,
        arena_allocator,
        json,
        .{},
    );

    const d_value = value.object.get("dependencies") orelse return Error.InvalidDependenciesJSON;
    dependencies.dependencies = try std.json.parseFromValueLeaky(
        [][]const u8,
        arena_allocator,
        d_value,
        .{},
    );

    const t_value = value.object.get("targets") orelse return Error.InvalidDependenciesJSON;
    var t_iterator = t_value.object.iterator();
    while (t_iterator.next()) |entry| {
        const target = try std.json.parseFromValueLeaky(
            DependencyTarget,
            arena_allocator,
            entry.value_ptr.*,
            .{},
        );

        try dependencies.targets.putNoClobber(entry.key_ptr.*, target);
    }

    return .{
        .arena = arena,
        .value = dependencies,
    };
}

// TODO: improve add_* functions to make them testable

fn add_include_dir(ctx: GetFlagsContext, dir: []const u8) !void {
    if (ctx.options.cflags_include) {
        const prefix = if (ctx.options.msvc) I_prefix_msvc else I_prefix;

        const flag = try std.mem.join(ctx.allocator, "", &.{ prefix, dir });
        try ctx.flags.put(flag, {});
    }
}

fn add_define(ctx: GetFlagsContext, define: []const u8) !void {
    if (ctx.options.cflags_define) {
        const prefix = if (ctx.options.msvc) D_prefix_msvc else D_prefix;

        const flag = try std.mem.join(ctx.allocator, "", &.{ prefix, define });
        try ctx.flags.put(flag, {});
    }
}

fn add_libdir(ctx: GetFlagsContext, libdir: []const u8) !void {
    if (ctx.options.libs_ldpath) {
        const prefix = if (ctx.options.msvc) L_prefix_msvc else L_prefix;

        const flag = try std.mem.join(ctx.allocator, "", &.{ prefix, libdir });
        try ctx.flags.put(flag, {});
    }
}

fn last_path_sep(path: []const u8) ?usize {
    for (0..path.len) |i| {
        const r_i = path.len - 1 - i;
        if (std.fs.path.isSep(path[r_i])) {
            return r_i;
        }
    }

    return null;
}

// libstr is likely to be a full name (libsomething.so) if there's a dot in it
fn libstr_is_full_name(libstr: []const u8) bool {
    return std.mem.indexOfScalar(u8, libstr, '.') != null;
}

fn parse_library(libstr: []const u8) !Library {
    if (libstr.len == 0) {
        return Error.InvalidLibrary;
    }

    var L: []const u8 = &.{};
    var l: []const u8 = &.{};

    var L_end: usize = 0;
    var l_start: usize = 0;

    const sep_i = last_path_sep(libstr);
    if (sep_i) |i| {
        if (builtin.os.tag == .windows) {
            // C:\
            //   ^
            if (i == 0 or (i == 2 and libstr[1] == ':')) {
                L_end = i + 1;
            } else {
                L_end = i;
            }
            l_start = i + 1;
        } else {
            //  /libname.so
            //  ^
            if (i == 0) {
                L_end = 1;
            } else {
                L_end = i;
            }
            l_start = i + 1;
        }
    }

    L = libstr[0..L_end];
    l = libstr[l_start..]; // full names are ok

    if (l.len == 0) {
        return Error.InvalidLibrary;
    }

    return .{
        .L = L,
        .l = l,
    };
}

fn add_library_by_libstr(ctx: GetFlagsContext, libstr: []const u8) !void {
    var l = libstr;
    var prefix: []const u8 = "";

    var l_p: []const u8 = undefined;
    var l_pfn: []const u8 = undefined;
    if (ctx.options.msvc) {
        l_p = l_prefix_msvc;
        l_pfn = l_prefix_full_name_msvc;
    } else {
        l_p = l_prefix;
        l_pfn = l_prefix_full_name;
    }

    // if this is an argument, pass as is
    if (!std.mem.startsWith(u8, libstr, "-")) {
        if (ctx.options.convert_libs) {
            const library = try parse_library(libstr);
            l = library.l;
            if (library.L.len != 0) {
                try add_libdir(ctx, library.L);
                prefix = l_pfn;
            } else {
                prefix = if (libstr_is_full_name(library.l)) l_pfn else l_p;
            }
        } else if (last_path_sep(libstr) == null) {
            prefix = l_p;

            if (libstr_is_full_name(libstr)) {
                prefix = l_pfn;
            }
        }
    }

    if (ctx.options.libs_lib) {
        const flag = try std.mem.join(ctx.allocator, "", &.{ prefix, l });
        _ = ctx.flags.orderedRemove(flag); // most linkers link from left to right
        try ctx.flags.put(flag, {});
    }
}

fn get_flags_recursive(
    ctx: GetFlagsContext,
    library: []const u8,
    link_only: bool,
    target_map: TargetMap,
) !void {
    const target = target_map.get(library);
    if (target) |value| {
        if (!link_only) {
            for (value.include_dirs) |dir| {
                try add_include_dir(ctx, dir);
            }
            for (value.defines) |define| {
                try add_define(ctx, define);
            }
        }

        for (value.link_dirs) |dir| {
            try add_libdir(ctx, dir);
        }

        if (value.library) |lib| {
            try add_library_by_libstr(ctx, lib);
        }

        for (value.link_libraries) |dep| {
            const dep_link_only = link_only or dep.link_only;
            try get_flags_recursive(ctx, dep.lib, dep_link_only, target_map);
        }
    } else {
        try add_library_by_libstr(ctx, library);
    }
}

fn get_flags_from_dependencies(
    allocator: std.mem.Allocator,
    dependencies: Dependencies,
    options: GetFlagsOptions,
) !Flags {
    var arena = std.heap.ArenaAllocator.init(allocator);
    errdefer arena.deinit();

    const arena_allocator = arena.allocator();

    var flags = FlagSet.init(arena_allocator);

    const ctx = GetFlagsContext{
        .allocator = arena_allocator,
        .flags = &flags,
        .options = options,
    };

    for (dependencies.dependencies) |target_name| {
        try get_flags_recursive(
            ctx,
            target_name,
            false,
            dependencies.targets,
        );
    }

    return .{
        .arena = arena,
        .flags = flags.keys(),
    };
}

fn write_file(dir: std.fs.Dir, filename: []const u8, contents: []const u8) !void {
    var file = try dir.createFile(filename, .{});
    defer file.close();

    try file.writeAll(contents);
}

pub fn get_flags(
    allocator: std.mem.Allocator,
    targets: []const Target,
    options: GetFlagsOptions,
) !Flags {
    var dir = try temp.create_dir(allocator, "cmake-pkg-*");
    defer dir.deinit();

    var dir_handle = try dir.open(.{});
    defer dir_handle.close();

    {
        try write_file(dir_handle, cmake_lists_filename, cmake_lists_content);
        try write_file(dir_handle, genex_filename, genex_content);

        const targets_contents = try generate_targets_json(
            allocator,
            targets,
            options,
        );
        defer allocator.free(targets_contents);

        try write_file(dir_handle, targets_filename, targets_contents);

        const cache_contents = try generate_initial_cache(allocator, options);
        defer allocator.free(cache_contents);

        try write_file(dir_handle, cache_filename, cache_contents);
    }

    try call_cmake(allocator, dir_handle, options.silent);

    var dependencies: ParsedDependencies = undefined;
    {
        var dependencies_file = try dir_handle.openFile(dependencies_filename, .{});
        defer dependencies_file.close();

        const dependencies_json = try dependencies_file.readToEndAlloc(
            allocator,
            dependencies_max_bytes,
        );
        defer allocator.free(dependencies_json);

        dependencies = try parse_dependencies_json(allocator, dependencies_json);
    }
    defer dependencies.deinit();

    return try get_flags_from_dependencies(allocator, dependencies.value, options);
}

fn test_parse_target(target: []const u8, package: []const u8, name: []const u8) !void {
    const parsed_target = try parse_target(target);
    try std.testing.expectEqualStrings(package, parsed_target.package);
    try std.testing.expectEqualStrings(name, parsed_target.name);
}

fn test_parse_target_failure(target: []const u8) !void {
    try std.testing.expectError(Error.InvalidTarget, parse_target(target));
}

fn test_parse_variable(variable: []const u8, name: []const u8, value: []const u8) !void {
    const parsed_variable = try parse_variable(variable);
    try std.testing.expectEqualStrings(name, parsed_variable.name);
    try std.testing.expectEqualStrings(value, parsed_variable.value);
}

fn test_parse_variable_failure(variable: []const u8) !void {
    try std.testing.expectError(Error.InvalidVariable, parse_variable(variable));
}

fn test_validate_target_failure(target: Target) !void {
    try std.testing.expectError(Error.InvalidTarget, validate_target(target));
}

fn test_targets_json_generation(targets: []const Target, options: GetFlagsOptions, expected: []const u8) !void {
    const generated = try generate_targets_json(
        std.testing.allocator,
        targets,
        options,
    );
    defer std.testing.allocator.free(generated);
    try std.testing.expectEqualStrings(expected, generated);
}

fn test_cmake_string_encoding(str: []const u8, expected: []const u8) !void {
    const encoded = try encode_cmake_string(std.testing.allocator, str);
    defer std.testing.allocator.free(encoded);

    try std.testing.expectEqualStrings(expected, encoded);
}

fn test_cache_generation(options: GetFlagsOptions, expected: []const u8) !void {
    const cache = try generate_initial_cache(std.testing.allocator, options);
    defer std.testing.allocator.free(cache);

    try std.testing.expectEqualStrings(expected, cache);
}

test "target parsing" {
    try test_parse_target(
        "Package::target",
        "Package",
        "target",
    );
    try test_parse_target(
        "Package::complex::target",
        "Package",
        "complex::target",
    );

    try test_parse_target_failure("");
    try test_parse_target_failure("Package");
    try test_parse_target_failure("Package::");
    try test_parse_target_failure("::target");
    try test_parse_target_failure("::complex::target");
    try test_parse_target_failure("Package::invalid::complex::target");
}

test "variable parsing" {
    try test_parse_variable(
        "VARIABLE=",
        "VARIABLE",
        "",
    );
    try test_parse_variable(
        "VARIABLE=VALUE",
        "VARIABLE",
        "VALUE",
    );
    try test_parse_variable(
        "VARIABLE=VA=LUE",
        "VARIABLE",
        "VA=LUE",
    );

    try test_parse_variable_failure("");
    try test_parse_variable_failure("VARIABLE");
    try test_parse_variable_failure("=");
    try test_parse_variable_failure("=VALUE");
}

test "target validating" {
    try validate_target(.{
        .package = "Package",
        .name = "target",
    });
    try validate_target(.{
        .package = "Package",
        .name = "complex::target",
    });

    try test_validate_target_failure(.{
        .package = "",
        .name = "",
    });
    try test_validate_target_failure(.{
        .package = "Package",
        .name = "",
    });
    try test_validate_target_failure(.{
        .package = "",
        .name = "target",
    });
    try test_validate_target_failure(.{
        .package = "Package",
        .name = "::",
    });
    try test_validate_target_failure(.{
        .package = "Package",
        .name = "::target",
    });
    try test_validate_target_failure(.{
        .package = "Package",
        .name = "target::",
    });
    try test_validate_target_failure(.{
        .package = "Package",
        .name = "invalid::complex::target",
    });
}

test "targets.json generating" {
    try test_targets_json_generation(&.{}, .{},
        \\{"config":"ANY","targets":[]}
    );
    try test_targets_json_generation(&.{
        .{ .package = "OpenSSL", .name = "SSL" },
        .{ .package = "OpenSSL", .name = "Crypto" },
    }, .{},
        \\{"config":"ANY","targets":[{"package":"OpenSSL","name":"SSL"},{"package":"OpenSSL","name":"Crypto"}]}
    );
    try test_targets_json_generation(&.{
        .{ .package = "OpenSSL", .name = "Crypto" },
        .{ .package = "OpenSSL", .name = "SSL" },
    }, .{},
        \\{"config":"ANY","targets":[{"package":"OpenSSL","name":"Crypto"},{"package":"OpenSSL","name":"SSL"}]}
    );
    try test_targets_json_generation(&.{
        .{ .package = "OpenSSL", .name = "SSL" },
        .{ .package = "OpenSSL", .name = "SSL" },
    }, .{},
        \\{"config":"ANY","targets":[{"package":"OpenSSL","name":"SSL"}]}
    );
    try test_targets_json_generation(&.{}, .{
        .config = .Release,
    },
        \\{"config":"RELEASE","targets":[]}
    );
    try test_targets_json_generation(&.{}, .{
        .config = .Debug,
    },
        \\{"config":"DEBUG","targets":[]}
    );
    try test_targets_json_generation(&.{}, .{
        .config = .None,
    },
        \\{"config":"NONE","targets":[]}
    );
    try test_targets_json_generation(&.{}, .{
        .config = .RelWithDebInfo,
    },
        \\{"config":"RELWITHDEBINFO","targets":[]}
    );
    try test_targets_json_generation(&.{}, .{
        .config = .MinSizeRel,
    },
        \\{"config":"MINSIZEREL","targets":[]}
    );
    try test_targets_json_generation(&.{
        .{ .package = "OpenSSL", .name = "SSL" },
        .{ .package = "zstd", .name = "libzstd" },
    }, .{},
        \\{"config":"ANY","targets":[{"package":"OpenSSL","name":"SSL"},{"package":"zstd","name":"libzstd"}]}
    );

    try std.testing.expectError(
        error.OutOfMemory,
        generate_targets_json(
            std.testing.failing_allocator,
            &.{
                .{ .package = "OpenSSL", .name = "SSL" },
            },
            .{},
        ),
    );
}

fn test_library_parsing(libstr: []const u8, L: []const u8, l: []const u8) !void {
    const library = try parse_library(libstr);
    try std.testing.expectEqualStrings(L, library.L);
    try std.testing.expectEqualStrings(l, library.l);
}

test "cmake string encoding" {
    try test_cmake_string_encoding(
        \\test
    ,
        \\"test"
    );
    try test_cmake_string_encoding(
        \\test test
    ,
        \\"test test"
    );
    try test_cmake_string_encoding(
        \\te"st
    ,
        \\"te\"st"
    );
    try test_cmake_string_encoding(
        \\test
        \\test
    ,
        \\"test
        \\test"
    );

    try std.testing.expectError(error.OutOfMemory, encode_cmake_string(
        std.testing.failing_allocator,
        "string",
    ));
}

test "cache generation" {
    try test_cache_generation(.{},
        \\
    );
    try test_cache_generation(.{
        .paths = &.{"/test/path"},
        .vars = &.{.{ .name = "name", .value = "value" }},
    },
        \\set(CMAKE_PREFIX_PATH "/test/path" CACHE PATH "")
        \\set("name" "value" CACHE INTERNAL "")
        \\
    );
    try test_cache_generation(.{
        .paths = &.{ "/test/path", "/test/path_2", "/path with spaces and \"" },
        .vars = &.{
            .{
                .name = "name",
                .value = "value with spaces,\n and \"",
            },
            .{
                .name = "other_name",
                .value = "other_value",
            },
        },
    },
        \\set(CMAKE_PREFIX_PATH "/test/path;/test/path_2;/path with spaces and \"" CACHE PATH "")
        \\set("name" "value with spaces,
        \\ and \"" CACHE INTERNAL "")
        \\set("other_name" "other_value" CACHE INTERNAL "")
        \\
    );

    try std.testing.expectError(error.OutOfMemory, generate_initial_cache(
        std.testing.failing_allocator,
        .{ .paths = &.{"/test/path"} },
    ));
}

test "library parsing" {
    if (builtin.target.os.tag == .windows) {
        try test_library_parsing("C:\\crypto.lib", "C:\\", "crypto.lib");
        try test_library_parsing("C:\\lib\\crypto.lib", "C:\\lib", "crypto.lib");
    }

    try test_library_parsing("/usr/lib/libcrypto.so.3", "/usr/lib", "libcrypto.so.3");
    try test_library_parsing("/libcrypto.so.3", "/", "libcrypto.so.3");
    try test_library_parsing("crypto", "", "crypto");

    try std.testing.expectError(error.InvalidLibrary, parse_library(""));
}

test "dependencies.json parsing" {
    // TODO
}

test "flags from dependencies" {
    // TODO
}

test "flags from dependencies MSVC" {
    // TODO
}
