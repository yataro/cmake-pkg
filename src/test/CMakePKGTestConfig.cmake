add_library(CMakePKGTest::cmakepkgtestimpl STATIC IMPORTED)
set_target_properties(CMakePKGTest::cmakepkgtestimpl PROPERTIES
    IMPORTED_CONFIGURATIONS RELEASE
    INTERFACE_INCLUDE_DIRECTORIES "/cmakepkgtestimpl"
    INTERFACE_COMPILE_DEFINITIONS "cmakepkgtestimpl;IS_IMPL=1"
    IMPORTED_LOCATION_RELEASE "/libcmakepkgtestimpl.a"
)

add_library(CMakePKGTest::cmakepkgtest STATIC IMPORTED)
set_target_properties(CMakePKGTest::cmakepkgtest PROPERTIES
    IMPORTED_CONFIGURATIONS RELEASE
    INTERFACE_INCLUDE_DIRECTORIES "/cmakepkgtest"
    INTERFACE_COMPILE_DEFINITIONS "cmakepkgtest;IS_MAIN=1"
    IMPORTED_LOCATION_RELEASE "/libcmakepkgtest.a"
    INTERFACE_LINK_LIBRARIES "CMakePKGTest::cmakepkgtestimpl"
)

add_library(CMakePKGTest::CMakePKGTest INTERFACE IMPORTED)
set_target_properties(CMakePKGTest::CMakePKGTest PROPERTIES
    INTERFACE_LINK_LIBRARIES "CMakePKGTest::cmakepkgtest"
)
