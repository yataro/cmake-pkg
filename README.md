# cmake-pkg

pkg-config clone for CMake packages.

This program tries to mimic pkg-config interface with CMake packages.
Can handle essential generator expressions, enough to be used with packages like Qt6.

## Usage

```
Usage:
    cmake-pkg [OPTIONS] <TARGET>...
Options:
    -h, --help              print this message and exit
    -V, --version           print cmake-pkg version and exit
    --cflags                print compiler flags
    --cflags-only-I         print include-dir flags
    --cflags-only-D         print define flags
    --libs                  print linker flags
    --libs-only-L           print LDPATH linker flags
    --libs-only-l           print LIBNAME linker flags
    --convert-libs          convert libraries from absolute paths to -L -l form
    --with-path <PATH>...   add a directory to the CMAKE_PREFIX_PATH
    --config <CONFIG>...    specify configuation (defaults to release or any other available)
    -D <VARIABLE>...        define variable entry
    -s, --silent            silent mode
    --msvc                  print flags in MSVC syntax
    <TARGET>...             target(s) in format Package::target
```

## Example

```
$ cmake-pkg --silent --cflags --libs OpenSSL::SSL zstd::libzstd
-I/usr/include /usr/lib/libssl.so /usr/lib/libcrypto.so /usr/lib/libzstd.so.1.5.6
```

Components can be specified with `<PACKAGE>_COMPONENTS` variable

```
$ cmake-pkg --silent --cflags --libs -DQt6_COMPONENTS="Core;Network" Qt6::Core Qt6::Network
-I/usr/include/qt6/QtCore -I/usr/include/qt6 -DQT_CORE_LIB -DQT_NO_DEBUG -I/usr/lib/qt6/mkspecs/linux-g++ -I/usr/include/qt6/QtNetwork -DQT_NETWORK_LIB /usr/lib/libQt6Network.so.6.7.1 /usr/lib/libQt6Core.so.6.7.1
```

# Supported generator expressions

 - `$<condition:true_string>`
 - `<IF:condition,true_string,false_string>`
 - `$<BOOL:string>`
 - `$<AND:conditions>`
 - `$<OR:conditions>`
 - `$<NOT:condition>`
 - `$<STREQUAL:string1,string2>`
 - `$<EQUAL:value1,value2>`
 - `$<VERSION_LESS:v1,v2>`
 - `$<VERSION_GREATER:v1,v2>`
 - `$<VERSION_EQUAL:v1,v2>`
 - `$<VERSION_LESS_EQUAL:v1,v2>`
 - `$<VERSION_GREATER_EQUAL:v1,v2>`
 - `$<LOWER_CASE:string>`
 - `$<UPPER_CASE:string>`
 - `$<MAKE_C_IDENTIFIER:...>`
 - `$<IN_LIST:string,list>`
 - `$<CONFIG>`
 - `$<CONFIG:cfgs>`
 - `$<LINK_ONLY:...>`
 - `$<TARGET_EXISTS:tgt>`
 - `$<TARGET_NAME_IF_EXISTS:tgt>`
 - `$<TARGET_POLICY:policy>` (dummy)
 - `$<TARGET_PROPERTY:tgt,prop>`
 - `$<TARGET_PROPERTY:prop>` (dummy)
 - `$<ANGLE-R>`
 - `$<COMMA>`
 - `$<SEMICOLON>`

## Why

Some time ago I needed to use a complex CMake-only library from a simple Makefile. I found that there is `cmake --find-package`, but it is deperacted, too complex to use and doesn't work with most targets.
I decided to write pkg-config mimic just for fun and to learn zig language.
